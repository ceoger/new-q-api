class ApplicationController < ActionController::API

  def is_a_valid_email?(email)
      (/\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i =~ email) != nil
    end

    def not_found
      render json: {error: 'not_found'}
    end

    def authorize_request
      header = request.headers['Authorization']
      header = header.split(' ').last if header
      begin
        @decoded = JsonWebToken.decode(header)
        @current_user = User.find(@decoded[:user_id])
      rescue Active::RecordNotFound => e
        render json: { error: e.messsage }, status: :unauthorized
      rescue JWT::DecodeError => e
        render json: { error: e.messsage }, status: :unauthorized
      end
    end
end
